This bash script was created to install all applications below on first 'vagrant up'

Config variables:

Line 14 - MYSQL_ROOT, for your user to MySQL

Line 15 - MYSQL_PASS, for your pass to MySQL

Line 18 - TIMEZONE, your locale




[APPLICATIONS]



vim

curl

wget

git

make # make is not installed by default believe it or not

openssl # openssl will allow https connections

a2enmod ssl # ssl/https enable

unzip # unzip .zip files from cli


Apache 2

-> mod_rewrite

-> AllowOverride None to AllowOverride All


MongoDB


PHP 5

-> Composer

-> php5-cli

-> php5-common

-> php5-curl

-> php5-imap

-> php5-mcrypt

-> php5-mysql

-> php5-sqlite

-> php5-xdebug

-> php5-gd

-> libapache2-mod-php5

-> php5-dev

-> php5-imagick

-> php-pear


MySQL

-> Comment "bind-address" in /etc/mysql/my.cnf

-> GRANT ALL PRIVILEGES to root


NodeJS

-> mongoose

-> express
