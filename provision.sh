#marcelo a. b. elias (marckecho) | back-end developer
#marckecho@gmail.com

#!/usr/bin/env bash
# Boxes do Vagrant ::  http://www.vagrantbox.es/

#VARIABLES CONFIG 

### MYSQL
MYSQL_ROOT="root"
MYSQL_PASS="vagrant"

### Locale
TIMEZONE="America/Sao_Paulo"

# END VARIABLES CONFIG #


# Root user
sudo su

#atualiza o apt-get
apt-get update

# Time Zone & Locale
apt-get install -y ntp
echo $TIMEZONE | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata

# Tools
apt-get install -y vim # Vim
apt-get install -y curl # cURL
apt-get install -y wget # wGET
apt-get install -y git # GIT
apt-get install -y make # make is not installed by default believe it or not
apt-get install -y openssl # openssl will allow https connections
a2enmod ssl # ssl/https enable
apt-get install -y unzip # unzip .zip files from cli

# APACHE
apt-get install -y apache2
sed -i '/AllowOverride None/c AllowOverride All' /etc/apache2/sites-enabled/000-default #alteração para o mod_rewrite

#ajusta o ServerName
echo "ServerName localhost" > /etc/apache2/httpd.conf

#remove a pasta abaixo
rm -rf /var/www

#cria um link de vagrant para a pasta /var/www
ln -fs /vagrant/www /var/www

#create folder for MONGODB data
sudo mkdir /vagrant/data/
sudo mkdir /vagrant/data/db/

# MOD_REWRITE
a2enmod rewrite

# MongoDB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install mongodb-10gen
apt-get install mongodb-10gen=2.2.3
echo "mongodb-10gen hold" | sudo dpkg --set-selections
echo "extension=mongo.so" > /etc/php5/conf.d/mongo.ini
echo "extension=mongo.so" > /etc/php5/apache2/php.ini

# Install PHP
apt-get install -y php5
# php install with common extensions
apt-get install -y php5-cli
apt-get install -y php5-common
apt-get install -y php5-curl
apt-get install -y php5-imap
apt-get install -y php5-mcrypt
apt-get install -y php5-mysql
apt-get install -y php5-mssql
apt-get install -y php5-sqlite
apt-get install -y php5-xdebug
apt-get install -y php5-gd
apt-get install -y libapache2-mod-php5
apt-get install -y php5-dev # phep5-dev to get phpize
apt-get install -y php5-imagick # imagik
apt-get install -y php-pear # pear

# MySQL
echo "mysql-server-5.5 mysql-server/root_password password $MYSQL_PASS" | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password $MYSQL_PASS" | debconf-set-selections
sudo apt-get install -y mysql-server # install mysql server and client

sed -i '/bind-address/c #bind-address' /etc/mysql/my.cnf  #alteração para aceitar qualquer host

mysql -u root -pvagrant -e "USE mysql;GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_ROOT'@'%' IDENTIFIED BY '$MYSQL_PASS';FLUSH PRIVILEGES;"

# Instala o driver do MongoDB para o PHP
# * Faz parte da configuração abaixo
sudo pecl install mongo

# Install Composer for PHP
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# NodeJS
sudo apt-get install python-software-properties python g++ make
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs

# NodeJS - NPM
curl http://npmjs.org/install.sh | sh
npm install express
npm install mongoose

# Clean up
sudo dpkg --configure -a # when upgrade or install doesnt run well, this may resolve quite a few issues
apt-get autoremove -y # remove obsolete packages

# Restart Services
service apache2 restart
service mysql restart
service mongodb restart

ifconfig